import { Appointment } from "./appointment";

export class User {
  id: number;
  username: string;
  password: string;
  appointments: Appointment[];
}
