export class ValidationPostbody {
    valid: boolean;
    uuid: string;
    constructor(valid: boolean, uuid:string){
        this.valid = valid;
        this.uuid = uuid;
    }
}
