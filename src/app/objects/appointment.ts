export class Appointment {
  id: number;
  telephone: string;
  uuid: string;
  dateTime: string;
}
