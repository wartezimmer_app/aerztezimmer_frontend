import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Appointment } from '../objects/appointment';
import { ValidationPostbody } from '../objects/validationpostbody';
import { LoadAppointmentByUuidPostbody } from '../objects/loadappointmentrequestbody';
@Injectable({
  providedIn: 'root'
})
export class AppointmentService extends BaseService<Appointment> {
  constructor(
    http: HttpClient) {
    super(http, 'appointments');
  }

  getAllAppointments(): Observable<any> {
    return this.http.get(this.requestURL + '/search/personal', BaseService.HTTP_OPTIONS);
  }

  getTodaysAppoinments(): Observable<any> {
    return this.http.get(this.requestURL + '/search/today', BaseService.HTTP_OPTIONS);
  }

  createAppointment(name, telephone, date, waytime): Observable<any> {
    return this.http.put(this.requestURL + '/create', {
      'name': name,
      'telephone': telephone,
      'dateTime': date,
      'waytime': waytime,
    }, BaseService.HTTP_OPTIONS);
  }

  deleteAppointment(uuid): Observable<any> {
    return this.http.patch(this.requestURL + '/remove/byUuid', {
      'uuid': uuid
    }, BaseService.HTTP_OPTIONS);
  }

  delayAppointments(minutes): Observable<any> {
    return this.http.patch(this.requestURL + '/update/delayAfterAppointment', {
      'minutes': minutes
    }, BaseService.HTTP_OPTIONS);
  }

  validateAppointment(uuid, valid): Observable<any> {
    return this.http.post(this.requestURL + '/update/appointmentValidation', new ValidationPostbody(valid, uuid), BaseService.HTTP_OPTIONS);
  }

  triggerSms(uuid): Observable<any> {
    return this.http.post(this.requestURL + '/triggerSms', new LoadAppointmentByUuidPostbody(uuid), BaseService.HTTP_OPTIONS);
  }
}
