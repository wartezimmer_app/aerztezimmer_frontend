import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { User } from '../objects/user';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService<User> {
  constructor(
    http: HttpClient) {
    super(http, "users");
  }

  public login(): Observable<User> {
    return this.http.post<User>(this.requestURL + "/login", [], BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleError(this.entity + ':findAll'))
      );
  }
}
