import {environment} from '../../environments/environment';

import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

const BASE_URL = environment.baseUrl;

export class BaseService<T> {
  static username = null;
  static password = null;
  static isLoggedIn = false;

  static HTTP_OPTIONS = {
    withCredentials: true,
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + btoa(":")
    })
  };

  static setUserAndPassword(username: string, password: string) {
    BaseService.username = username;
    BaseService.password = password;
    BaseService.HTTP_OPTIONS = {
      withCredentials: true,
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(BaseService.username + ':' + BaseService.password)
      })
    };
  }

  protected requestURL = BASE_URL;  // URL to web api

  constructor(
    protected http: HttpClient,
    protected entity
  ) {
    this.requestURL = BASE_URL + entity;
  }

  protected handleError(operation = 'operation') {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      //      return of(error);
      return throwError(error);
    };
  }

  protected handleNumberError(operation = 'operation') {
    return (error: any): Observable<number> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      //      return of(error);
      return throwError(error);
    };
  }

  protected handleListError(operation = 'operation') {
    return (error: any): Observable<T[]> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      //      return of(error);
      return throwError(error);
    };
  }

  public findAll(): Observable<T[]> {
    return this.http.get<T[]>(this.requestURL, BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleListError(this.entity + ':findAll'))
      );
  }

  public create(t: T): Observable<T> {
    return this.http.put<T>(this.requestURL + '/-1', t, BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleError(this.entity + ':create'))
      );
  }

  public read(id: number): Observable<T> {
    return this.http.get<T>(this.requestURL + '/' + id, BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleError(this.entity + ':read'))
      );
  }

  public update(id: number, t: T): Observable<T> {
    return this.http.patch<T>(this.requestURL + '/' + id, t, BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleError(this.entity + ':update'))
      );
  }

  public delete(id: number): Observable<T> {
    return this.http.delete<T>(this.requestURL + '/' + id, BaseService.HTTP_OPTIONS)
      .pipe(
        catchError(this.handleError(this.entity + ':delete'))
      );
  }

}
