import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {EventComponent} from './components/event/event.component';
import {OverviewComponent} from './overview/overview.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import {ToastrModule} from 'ng6-toastr-notifications';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CreateAppointmentFormComponent} from './components/forms/create-appointment-form/create-appointment-form-component';
import {LinkApprovementComponent} from './userLinkApprovement/linkapprovement.component';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { ImpressumComponent } from './impressum/impressum.component';

import {JsClippyModule} from 'js-clippy';
import { LandingComponent } from './landing/landing.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    EventComponent,
    LoginComponent,
    CreateAppointmentFormComponent,
    LinkApprovementComponent,
    ImpressumComponent,
    LandingComponent
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,

    MDBBootstrapModule.forRoot(),

    ToastrModule.forRoot(),

    BrowserAnimationsModule,

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    JsClippyModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
