import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'angular-bootstrap-md';
import { FormControl } from '@angular/forms';
import { UserService } from './services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { BaseService } from './services/base.service';
import { ClippyService } from 'js-clippy';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  languages: object = [
    {
      value: "de",
      label: "Deutsch"
    },
    {
      value: "en",
      label: "English"
    }
  ]

  openPages: string[] = [
    "#/impressum",
    "#/validate/",
    "#/"
  ];

  constructor(
    private userService: UserService,
    private router: Router,
    private translate: TranslateService,
    private clippy: ClippyService
  ) {
    translate.addLangs(['de', 'en']);
    translate.setDefaultLang('de');
    translate.use('de');
  }

  ngOnInit() {
    if (!this.checkUrlPrefix()) {
      this.router.navigate(['/']);
    }
    console.log(this.openPages.indexOf(window.location.hash) == -1);
    if (this.openPages.indexOf(window.location.hash) == -1) {
    }
  }

  checkUrlPrefix(): boolean {
    for (let entry of this.openPages) {
      if (window.location.hash.startsWith(entry)) {
        return true;
      }
    }
    return false;
  }

  logout(): void {
    BaseService.setUserAndPassword(null, null);
    BaseService.isLoggedIn = false;
    this.router.navigate(['/']);
  }

  isLoggedIn(): boolean {
    return BaseService.isLoggedIn;
  }

  clippyExists: boolean = false;
  clippyIsShown: boolean = false;

  showClippy(): void {
    if (this.clippyExists == false) {
      this.clippy.create("Clippy");
      this.clippyExists = true;
    } else if (this.clippyIsShown == false) {
      this.clippy.show(true);
      this.clippyIsShown = true;
    } else {
      if (Math.random() <= 0.1) {
        this.clippy.speak("How may I help?", false);
      } else {
        var animations = this.clippy.agent.animations();
        var randIndex = Math.floor(Math.random() * animations.length);
        console.log("Playing " + animations[randIndex]);
        this.clippy.agent.play(animations[randIndex]);
      }
    }
  }
}
