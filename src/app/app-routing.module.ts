import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EventComponent } from './components/event/event.component';
import { OverviewComponent } from './overview/overview.component';
import { LinkApprovementComponent } from './userLinkApprovement/linkapprovement.component';
import { ImpressumComponent } from './impressum/impressum.component';
import {LandingComponent} from './landing/landing.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: '', component: LandingComponent },
  { path: 'impressum', component: ImpressumComponent },
  { path: 'main', component: OverviewComponent },
  { path: 'validate/:uuid', component: LinkApprovementComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
