import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkApprovementComponent } from './linkapprovement.component';

describe('LinkApprovement', () => {
    let component: LinkApprovementComponent;
    let fixture: ComponentFixture<LinkApprovementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LinkApprovementComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LinkApprovementComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
