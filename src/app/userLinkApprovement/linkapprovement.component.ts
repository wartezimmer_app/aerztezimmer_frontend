import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'angular-bootstrap-md';
import { FormControl } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';
import { AppointmentService } from '../services/appointment.service';

@Component({
    selector: 'app-link',
    templateUrl: './linkapprovement.component.html',
    styleUrls: ['./linkapprovement.component.scss']
})
export class LinkApprovementComponent {
    @ViewChild(ModalDirective) modal: ModalDirective;

    isSelectionAllowed = true;
    timeInput = new FormControl();
    subjectInput = new FormControl();
    locationInput = new FormControl();
    descriptionInput = new FormControl();

    events: Array<any> = [];

    constructor(
        private toastr: ToastrManager,
        private userService: UserService,
        private appointmentService: AppointmentService,
        private activatedRoute: ActivatedRoute,
        private router: Router) {
    }

    appointmentNo(): void {
        this.activatedRoute.paramMap.subscribe(param => {
            var uuid = param.get('uuid');
            this.responseFeedback();
            this.appointmentService.validateAppointment(uuid, false).subscribe(
                value => {
                    this.events = value;
                }
            );
        });
    }

    appointmentYes(): void {
        this.activatedRoute.paramMap.subscribe(param => {
            var uuid = param.get('uuid');
            this.responseFeedback();
            this.appointmentService.validateAppointment(uuid, true).subscribe(
                value => {
                    this.events = value;
                }
            );
        });

    }

    responseFeedback(): void {
        this.isSelectionAllowed = false;
        this.toastr.successToastr('Vielen Dank für Ihre Rückmeldung!');
    }
}
