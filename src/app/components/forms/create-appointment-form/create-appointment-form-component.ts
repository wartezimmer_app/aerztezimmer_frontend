import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AppointmentService } from '../../../services/appointment.service';
import { UserService } from '../../../services/user.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { OverviewComponent } from '../../../overview/overview.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-create-appointment-form',
  templateUrl: './create-appointment-form.component.html',
  styleUrls: ['./create-appointment-form-component.scss']
})
export class CreateAppointmentFormComponent implements OnInit {
  @Input() groupId: number;

  date: Date = new Date();

  fineForm = new FormGroup({
    date: new FormControl([this.date.getDate(), this.normalizeMonth(this.date.getMonth() + 1), this.date.getFullYear()].join('.')),
    time: new FormControl(''),
    name: new FormControl(''),
    telephone: new FormControl(''),
    waytime: new FormControl(''),
  });

  constructor(
    private userService: UserService,
    private toastr: ToastrManager,
    private overviewComponent: OverviewComponent,
    private appointmentService: AppointmentService,
    public translate: TranslateService
  ) {
  }

  ngOnInit() {
  }

  addAppointment(): void {
    const date = this.fineForm.get('date').value;
    const time = this.fineForm.get('time').value;
    const name = this.fineForm.get('name').value;
    const telephone = this.fineForm.get('telephone').value;
    const waytime = this.fineForm.get('waytime').value;


    // todo: use real username/password and date from datepicker
    this.appointmentService.createAppointment(name, telephone, this.convertDateTime(date + ' ' + time), waytime).subscribe(
      resp => {
        this.translate.get('MSG_APPOINTMENT_SUCCESFULLY_CREATED').subscribe((res: string) => {
          this.toastr.successToastr(res);
        });
        this.overviewComponent.reloadAppointments();
      }, error => {
        this.toastr.errorToastr('Fehler!');
      }
    );
  }


  normalizeMonth(number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }

  convertDateTime(dateTime): Date {
    var m = dateTime.match(/(\d+).(\d+).(\d+)\s+(\d+):(\d+)/);
    if (m == undefined) {
      this.translate.get('MSG_DATETIME_FORMAT_NOT_CORRECT').subscribe((res: string) => {
        this.toastr.errorToastr(res);
      });
    }
    return new Date(Date.UTC(+m[3], +m[2] - 1, +m[1], +m[4], +m[5], 0));
  }

}
