import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {BaseService} from '../services/base.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {Router} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';

  constructor(
    private userService: UserService,
    private toastr: ToastrManager,
    private router: Router,
    public translate: TranslateService) {
  }

  ngOnInit() {
  }

  login(): void {
    BaseService.setUserAndPassword(this.username, this.password);
    this.userService.login()
      .subscribe(
        (data) => {
          this.translate.get('MSG_LOGIN_SUCCESS').subscribe((res: string) => {
            this.toastr.successToastr(res);
          });
          BaseService.isLoggedIn = true;
          this.router.navigate(['/main']);
        },
        (err) => {
          if (err.status === 401) {
            this.translate.get('MSG_LOGIN_INVALID').subscribe((res: string) => {
              this.toastr.errorToastr(res);
            });
          } else {
            this.translate.get('MSG_FAILED').subscribe((res: string) => {
              this.toastr.errorToastr(res);
            });
          }
        }
      );
  }

}
