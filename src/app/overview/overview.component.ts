import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from 'angular-bootstrap-md';
import {UserService} from '../services/user.service';
import {AppointmentService} from '../services/appointment.service';
import {ToastrManager} from 'ng6-toastr-notifications';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  @ViewChild(ModalDirective) modal: ModalDirective;

  events: Array<any> = [];

  constructor(
    private userService: UserService,
    private toastr: ToastrManager,
    private router: Router,
    private appointmentService: AppointmentService,
    public translate: TranslateService) {
  }

  ngOnInit() {
    this.reloadAppointments();

    // polling for new appointments every 30 seconds
    setInterval(() => {
      if (this.router.url === '/main') {
        this.reloadAppointments();
      }
    }, 30000);
  }

  deleteEvent(event: any) {
    const itemIndex = this.events.findIndex(el => el === event);
    const itemToDelete = this.events.find((value, index) => index === itemIndex);
    this.appointmentService.deleteAppointment(itemToDelete.uuid).subscribe(
      value => {
        this.translate.get('MSG_APPOINTMENT_DELETED').subscribe((res: string) => {
          this.toastr.successToastr(res);
        });
        this.reloadAppointments();
      }, error => {
        this.translate.get('MSG_FAILED').subscribe((res: string) => {
          this.toastr.errorToastr(res);
        });
      });
  }

  actionEvent(event: any) {
    const itemIndex = this.events.findIndex(el => el === event);
    const actionItem = this.events.find((value, index) => index === itemIndex);
    this.appointmentService.triggerSms(actionItem.uuid).subscribe(
      value => {
        this.translate.get('MSG_PATIENT_NOTIFIED').subscribe((res: string) => {
          this.toastr.successToastr(res);
        });
        this.reloadAppointments();
      }, error => {
        this.translate.get('MSG_FAILED').subscribe((res: string) => {
          this.toastr.errorToastr(res);
        });
      });
  }

  delayAppointment(minutes) {
    this.appointmentService.delayAppointments(minutes).subscribe(
      value => {
        this.translate.get('MSG_ALL_APPOINTMENTS_MOVED').subscribe((res: string) => {
          this.toastr.successToastr(res);
        });
        this.reloadAppointments();
      }, error => {
        this.translate.get('MSG_FAILED').subscribe((res: string) => {
          this.toastr.errorToastr(res);
        });
      }
    );
  }

  reloadAppointments() {
    this.appointmentService.getTodaysAppoinments().subscribe(
      value => {
        this.events = value;
      }
    );
  }

  getToday(): string {
    const date = new Date();
    switch (date.getDay()) {
      case 0:
        return 'GENERIC_SUNDAY';
      case 1:
        return 'GENERIC_MONDAY';
      case 2:
        return 'GENERIC_TUESDAY';
      case 3:
        return 'GENERIC_WEDNESDAY';
      case 4:
        return 'GENERIC_THURSDAY';
      case 5:
        return 'GENERIC_FRIDAY';
      case 6:
        return 'GENERIC_SATURDAY';
    }
  }
}
